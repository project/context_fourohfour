<?php
/**
 * @file
 * Context fourohfour reaction plugin for Context API.
 */

class context_fourohfour_reaction extends context_reaction {
  /**
   * Implements options_form().
   */
  public function options_form($context) {
    $values = $this->fetch_from_context($context);
    $form['#tree'] = TRUE;

    $form['type'] = array(
      '#title' => t('Fourohfour Type'),
      '#type' => 'select',
      '#default_value' => isset($values['type']) ? $values['type'] : 404,
      '#description' => t('The type of fourohfour to issue. You can !link.', array('!link' => l('review HTTP codes here', 'http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.3'))),
      '#options' => array(
        400 => '400: Bad Request',
        403 => '403: Forbidden',
        404 => '404: Not Found',
        410 => '410: Gone',
      ),
    );
    return $form;
  }


  /**
   * Implements execute().
   */
  public function execute() {
    $contexts = $this->get_contexts();
    foreach ($contexts as $context) {
      if (!empty($context->reactions[$this->plugin])) {
        $type =$context->reactions[$this->plugin]['type'];
        watchdog('context_fourohfour', 'User was fourohfoured with the @context context, from @from | $type', array('@context' => $context->name, '@from' => current_path(), '@type' => $type ), WATCHDOG_NOTICE, l("Configure {$context->name} context", "admin/structure/context/list/{$context->name}"));
        http_response_code($type);
        drupal_exit();
      }
    }
  }

}
